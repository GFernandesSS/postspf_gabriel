class Post < ActiveRecord::Base
  acts_as_votable
	belongs_to :user
	validates :content, presence: true, uniqueness: true
  validates :data, presence: true
end
